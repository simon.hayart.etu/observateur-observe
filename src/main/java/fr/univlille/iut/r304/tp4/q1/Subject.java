package fr.univlille.iut.r304.tp4.q1;


public abstract class Subject {

	protected void notifyObservers() {
	}

	protected void notifyObservers(Object data) {
	}

	public void attach(Observer observer) {
	}

	public void detach(Observer observer) {
	}
}
