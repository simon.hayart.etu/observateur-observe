package fr.univlille.iut.r304.tp4.q1;


public class Timer {

	public void start() {
	}

	public void stopRunning() {
	}

	public void attach(Observer observer) {
		// methode cree pour que les tests compilent sans erreur
		// n'est pas censée rester une fois que vous avez fini Q1.3
	}

	public void detach(Observer observer) {
		// methode cree pour que les tests compilent sans erreur
		// n'est pas censée rester une fois que vous avez fini Q1.3
	}

}
